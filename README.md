# GraphQL React Monorepo Typescript Boilerplate 

[![pipeline status](https://gitlab.com/mimukit/graphql-react-monorepo-typescript/badges/master/pipeline.svg)](https://gitlab.com/mimukit/graphql-react-monorepo-typescript/commits/master) [![Netlify Status](https://api.netlify.com/api/v1/badges/c1392a6c-3ddb-400f-a0ed-9da9ba0e3df8/deploy-status)](https://app.netlify.com/sites/graphql-react-monorepo/deploys) [![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg)](https://gitlab.com/mimukit/graphql-react-monorepo-typescript/merge_requests) [![License](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/mimukit/graphql-react-monorepo-typescript/blob/master/LICENSE) 


A full stack boilerplate for GraphQL & React with automated CI/CD with [Gitlab CI](https://about.gitlab.com/product/continuous-integration/).


## Prerequisites

- [Docker CE](https://www.docker.com/get-started)
- [Node](https://nodejs.org/en/)
- [Yarn](https://yarnpkg.com/en/)
- [GitLab](https://about.gitlab.com/)
- [Netlify](https://www.netlify.com)
- [Now.sh](https://zeit.co/now) or [Heroku](https://www.heroku.com)



## Installation

- Clone repo: `git clone {repo_url} {project_folder_name}`
- Run: `cd {project_folder_name}`
- Switch to *develop* branch: `git checkout develop`
- Run: `yarn install` 
- Rename `.env.example` to `.env` on both directory ***packages/server*** & ***packages/web*** and fill with correct environment variables
- Run `yarn build:common`
- Run `yarn dev`
- Enjoy 🍻🤟💕


## Configure Gitlab & CI/CD

- Create a project repo on [gitlab](https://gitlab.com/projects/new)
- Setup CI/CD ( project > setting > ci_cd ) ***environment variables***:

	- HEROKU_AUTH_TOKEN
	- HEROKU_PROD_SERVER
	- HEROKU_REGISTRY
	- HEROKU_STAGING_SERVER
	- HEROKU_USER
	- NETLIFY_AUTH_TOKEN
	- NETLIFY_PROD_ID
	- NETLIFY_PROD_SERVER
	- NETLIFY_STAGING_ID
	- NETLIFY_STAGING_SERVER
	- NOW_AUTH_TOKEN
	- NOW_PROD_SERVER
	- NOW_STAGING_SERVER
	- SERVER_DEPLOYMENT_SERVICE
	
*N.B: Mark all variables protected so that they are only available to protected branches.*

- Setup repository ( project > setting > repository ) ***protected branches***:

| Branch  | Allowed to merge 		 | Allowed to push 			| 
|---------|--------------------------|--------------------------|
| master  | maintainers 			 | no one		  			|
| staging | developers + maintainers | developers + maintainers |
| develop | developers + maintainers | developers + maintainers |

- Update ***remote url*** of the project with your new repo url: 
```
cd existing_repo
git remote rename origin old-origin
git remote add origin git@gitlab.com:{user}/{project-name}.git
git push -u origin --all
git push -u origin --tags
```

## Environment Variables

- **HEROKU_AUTH_TOKEN**

	- Go to heroku account [setting](https://dashboard.heroku.com/account)
	- Copy the **API Key**

![grmt-heroku-auth-api-key.png](https://i.ibb.co/51w1M98/grmt-heroku-auth-api-key.png)

- **HEROKU_PROD_SERVER**

	- Name of your production app on heroku

![grmt-heroku-prod-server.png](https://i.ibb.co/wwXRjB6/grmt-heroku-prod-server.png)

- **HEROKU_REGISTRY**

	- Heroku registry server address: `registry.heroku.com`

- **HEROKU_STAGING_SERVER**

	- Name of your staging app on heroku like the *HEROKU_PROD_SERVER* value

- **HEROKU_USER**

	-	Your heroku user email like `email@example.com`

- **NETLIFY_AUTH_TOKEN**

	- Go to netlify account [setting page](https://app.netlify.com/account/applications)
	- Create a **Personal access token**

![grmt-netlify-access-token.png](https://i.ibb.co/2ykrzyC/grmt-netlify-access-token.png)

- **NETLIFY_PROD_ID**

	- Create sites on netlify. See **[Config Netlify](#config-netlify)** section below
	- Go to setting page of your production site
	- Copy `API ID`

![grmt-netlify-site-info.png](https://i.ibb.co/XJ5GKRT/grmt-netlify-site-info.png)

- **NETLIFY_PROD_SERVER**

	- Go to setting page of your production site
	- Copy `Site name`

- **NETLIFY_STAGING_ID**

	- Go to setting page of your staging site
	- Copy `API ID`

- **NETLIFY_STAGING_SERVER**

	- Go to setting page of your staging site
	- Copy `Site name`

- **NOW_AUTH_TOKEN**

	- Go to now.sh [token setting](https://zeit.co/account/tokens) page
	- Create a token & copy that

- **NOW_PROD_SERVER**

	- now.sh `alias` of the production server
	
- **NOW_STAGING_SERVER**

	- now.sh `alias` of the staging server

- **SERVER_DEPLOYMENT_SERVICE**

	- Any one of these two values: `now` or `heroku`
	- It defines which hosting provider to use for the graphql api server deployment


## Config Heroku:

- Create a two new apps on [heroku](https://dashboard.heroku.com/new-app), one for `staging` & one for `production` server
- Set environment variables for both apps on `setting` page

![grmt-heroku-server-env-config.jpg](https://i.ibb.co/tBvNLgC/grmt-heroku-server-env-config.jpg)

- Server environment variable list:
	- FRONTEND_URL
	- NODE_ENV


## Config Netlify:

- We need two new apps on [netlify](netlify.com), one for `staging` & one for `production` version of web app
- First install the Netlify CLI tool : 
```
npm i -g netlify-cli
```
- Create sites using the following command:
```
netlify sites:create --name {your-site-name}
```
![grmt-netlify-create-app.png](https://i.ibb.co/KWctSxK/grmt-netlify-create-app.png)


## Config Now.sh:

- Install now.sh cli tool: 
```
npm i -g now
```
- Login with now.sh cli:
```
now login
```
- Add secrets to now.sh account for apps:
```
now secrets add {env_secret_name} "{env_secret_value}"
```
- Server environment variable list:
	- FRONTEND_URL

- Update `now.prod.json` and `now.staging.json` with those secret you set and proper settings
```
{
    "version": 1,
    "type": "docker",
    "public": true,
    "name": "graphql-react-monorepo",
    "alias": "graphql-react-monorepo",
    "env": {
        "NODE_ENV": "production",
        "FRONTEND_URL": "@env_secret_name_for_frontend-url"
    }
}

```

## TODOs
- [ ] deployment on digital ocean, vultr


*N.B: other feature request are welcomed. Just create an issue.*

## License
Released under the [MIT](https://gitlab.com/mimukit/graphql-react-monorepo-typescript/blob/master/LICENSE)
