import express from 'express';
import { ApolloServer } from 'apollo-server-express';
import { importSchema } from 'graphql-import';
import path from 'path';

import msg from '@monorepo/common';

const server = express();

const typeDefs = importSchema(
  path.join(__dirname, './schema/schema.graphql'),
) as any;

const resolvers = {
  Query: {
    hello: () => 'Hello world!!!',
    common: () => msg,
  },
};

const apollo = new ApolloServer({
  typeDefs,
  resolvers,
});

apollo.applyMiddleware({
  app: server,
  path: '/graphql',
  cors: {
    origin: process.env.FRONTEND_URL,
    optionsSuccessStatus: 200,
  },
});

export default server;
