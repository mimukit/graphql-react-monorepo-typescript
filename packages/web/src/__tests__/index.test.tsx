import React from 'react';
import ReactDOM from 'react-dom';
import App from '../App';

test('Web renders without crashing', () => {
  const root = document.createElement('root');
  ReactDOM.render(<App title="Hello World" />, root);
  ReactDOM.unmountComponentAtNode(root);
});
